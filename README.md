# PlatesExtension - plates-components

This is a [Plates](https://github.com/thephpleague/plates) template engine extension for easy build reusable components.

I like the Laravel template system Blade. Blade is good, native PHP is much better.
But Blade has a feature that is missing in Plates. The [components and slots](https://laravel.com/docs/5.5/blade#components-and-slots).
With this feature, it's more easier for build reusable components.
Sure, you can build your components without this feature.
But for many others, including me, it feels more familiar to think in components and slots.
You know that from frontend systems like vue.js.

## Table of contents

* [Usage](#usage)
* [Installation](#installation)
    * [Requirements](#requirements)
* [Contributing](#contributing)
* [Licence](#licence)

## Usage

To demonstrate how cool Components are, lets make an empty-state-container component on the top of [tachyons](http://tachyons.io/) and [Font Awesome](https://fontawesome.com/).

[Demo files](https://gitlab.com/daily-five/plates-components/tree/master/demo)

Structure:
```
/app
 |-- /templates
 |    |-- /main-layout.php
 |    `-- /empty-state-container.php
 `-- /index.php
```

File: `index.php`
```php
<?php
// Composer autoloader
require_once 'vendor/autoload.php';

// Create an instance of the template engine
$templateEngine = new \League\Plates\Engine('templates');

// Load the components extension
$templateEngine->loadExtension(new \DailyFive\PlatesExtensions\Components());

// Render the template
echo $templateEngine->render('main-layout', array());
```

File: `templates/main-layout.php`
```php
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Plates Components are cool</title>
    <link rel="stylesheet" href="https://unpkg.com/tachyons@4.9.1/css/tachyons.min.css"/>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body>
    <div class="mw6 center pv4">
        <?= $this->component('empty-state-container') ?>
            <?= $this->slot('icon') ?>
                <i class="fas fa-check"></i>
            <?= $this->endSlot() ?>

            <?= $this->slot('title') ?>
                Greate Job
            <?= $this->endSlot() ?>

            <?= $this->slot('message') ?>
                You've done all your work today!
            <?= $this->endSlot() ?>
        <?= $this->endComponent() ?>

        <?= $this->component('empty-state-container') ?>
            <?= $this->slot('icon') ?>
                <i class="fas fa-inbox"></i>
            <?= $this->endSlot() ?>

            <?= $this->slot('title') ?>
                Empty
            <?= $this->endSlot() ?>

            <?= $this->slot('message') ?>
                There are no projects you can handle!
            <?= $this->endSlot() ?>

            <?= $this->slot('action') ?>
                <a class="f6 link dim br1 ph3 pv2 mb2 dib white bg-dark-green" href="#0">Create a new project</a>
            <?= $this->endSlot() ?>
        <?= $this->endComponent() ?>
    </div>
</body>
</html>
```

File: `templates/empty-state-container.php`
```php
<div class="sans-serif ba b--black-20 mb4">
    <?php if (isset($icon)): ?>
        <div class="tc f-headline black-40 mt4"><?= $icon ?></div>
    <?php endif; ?>
    <?php if (isset($title)): ?>
        <div class="tc f1 black-50 mt4"><?= $title ?></div>
    <?php endif; ?>
    <?php if (isset($message)): ?>
        <div class="tc f3 mt3 mb5"><?= $message ?></div>
    <?php endif; ?>
    <?php if (isset($action)): ?>
        <div class="tc mb5"><?= $action ?></div>
    <?php endif; ?>
    <?= isset($slot) ? $slot : '' ?>
</div>
```

## Installation

Install via CLI...
```bash
composer require daily-five/plates-components
```

...or adding in composer.json
```json
{
    "require": {
        "daily-five/plates-components": "^1.0"
    }
}
```
If you have permission problems at installation, try this [solution](https://gitlab.com/daily-five/framework/issues/1)

### Requirements

This is a stand alone extension. No third party libraries are required.

## Contributing

Please see CONTRIBUTING for details.

## License

The Plates extension is licensed under the MIT license.