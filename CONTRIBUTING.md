# Contributing

Any type of feedback, pull request or issue is welcome.

## Guidelines

Sorry for now, but coming soon...

## Tests

To run the test suite, you need [Composer](http://getcomposer.org/) and [PHPUnit](https://phpunit.de/):
```bash
./vendor/bin/phpunit
```