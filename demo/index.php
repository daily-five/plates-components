<?php
/*
|---------------------------------------------------------------------------
| Composer autoload
|---------------------------------------------------------------------------
*/
require_once __DIR__ . '/../vendor/autoload.php';


/*
|---------------------------------------------------------------------------
| Template Processing
|---------------------------------------------------------------------------
*/
// Create an instance of the template engine
$templateEngine = new \League\Plates\Engine(__DIR__.'/templates');

// Load the components extension
$templateEngine->loadExtension(new \DailyFive\PlatesExtensions\Components());

// Render the template
echo $templateEngine->render('main-layout', array());