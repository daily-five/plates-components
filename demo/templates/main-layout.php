<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Plates Components are cool</title>
    <link rel="stylesheet" href="https://unpkg.com/tachyons@4.9.1/css/tachyons.min.css"/>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body>
    <div class="mw6 center pv4">
        <?= $this->component('empty-state-container') ?>
            <?= $this->slot('icon') ?>
                <i class="fas fa-check"></i>
            <?= $this->endSlot() ?>

            <?= $this->slot('title') ?>
                Greate Job
            <?= $this->endSlot() ?>

            <?= $this->slot('message') ?>
                You've done all your work today!
            <?= $this->endSlot() ?>
        <?= $this->endComponent() ?>

        <?= $this->component('empty-state-container') ?>
            <?= $this->slot('icon') ?>
                <i class="fas fa-inbox"></i>
            <?= $this->endSlot() ?>

            <?= $this->slot('title') ?>
                Empty
            <?= $this->endSlot() ?>

            <?= $this->slot('message') ?>
                There are no projects you can handle!
            <?= $this->endSlot() ?>

            <?= $this->slot('action') ?>
                <a class="f6 link dim br1 ph3 pv2 mb2 dib white bg-dark-green" href="#0">Create a new project</a>
            <?= $this->endSlot() ?>
        <?= $this->endComponent() ?>
    </div>
</body>
</html>