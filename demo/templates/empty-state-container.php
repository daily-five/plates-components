<div class="sans-serif ba b--black-20 mb4">
    <?php if (isset($icon)): ?>
        <div class="tc f-headline black-40 mt4"><?= $icon ?></div>
    <?php endif; ?>
    <?php if (isset($title)): ?>
        <div class="tc f1 black-50 mt4"><?= $title ?></div>
    <?php endif; ?>
    <?php if (isset($message)): ?>
        <div class="tc f3 mt3 mb5"><?= $message ?></div>
    <?php endif; ?>
    <?php if (isset($action)): ?>
        <div class="tc mb5"><?= $action ?></div>
    <?php endif; ?>
    <?= isset($slot) ? $slot : '' ?>
</div>