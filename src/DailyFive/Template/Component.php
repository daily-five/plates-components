<?php

namespace DailyFive\Template;

use League\Plates\Template\Template;
use League\Plates\Engine;

/**
 * Class Component
 * @package App\PlatesExtensions
 */
class Component
{
    /**
     * Component name; Same as Template name
     *
     * @var string
     */
    protected $name;

    /**
     * @var \League\Plates\Engine
     */
    protected $engine;

    /**
     * Template data
     *
     * @var array
     */
    protected $data;

    /**
     * Current Slot name
     *
     * @var string
     */
    protected $slot;

    /**
     * Slot data
     *
     * @var array
     */
    protected $slotData;

    /**
     * Flag for Component state.
     *
     * @var bool
     */
    protected $isWatching;

    /**
     * Component constructor.
     *
     * @param \League\Plates\Engine $engine
     * @param string                $name
     */
    public function __construct(
        Engine $engine,
        $name
    ) {
        $this->engine = $engine;
        $this->name = $name;
        $this->data = array();
        $this->slotData = array();
        $this->isWatching = false;
    }

    /**
     * Add data for the Component Template
     *
     * @param array $data
     *
     * @return void
     */
    public function data(array $data)
    {
        $this->data = array_merge($data);
    }

    /**
     * Start the Component section
     *
     * @return void
     */
    public function start()
    {
        if ($this->isWatching) {
            throw new \LogicException(
                "You try to start '{$this->name}' until it's already started."
            );
        }
        $this->isWatching = true;
        ob_start();
    }

    /**
     * Close the Component section
     *
     * @return void
     */
    public function end()
    {
        if (! $this->isWatching) {
            throw new \LogicException(
                "The Component '{$this->name}' has to be started before it ends."
            );
        }
        if ($this->slot) {
            throw new \LogicException(
                "Slot '{$this->slot}' has to be closed before you can close the Component '{$this->name}'."
            );
        }
        $this->slotData['slot'] = ob_get_clean();
        $this->isWatching = false;
    }

    /**
     * Render the Component and return the result
     *
     * @return string
     * @throws \Exception
     * @throws \Throwable
     */
    public function render()
    {
        if ($this->isWatching) {
            throw new \LogicException(
                "The Component '{$this->name}' has to be closed before it can be rendered."
            );
        }
        $template = $this->makeTemplate();
        $template->data($this->data);
        return $template->render($this->slotData);
    }

    /**
     * Start a slot section
     *
     * @param string $name
     *
     * @return void
     */
    public function startSlot($name)
    {
        if (! $this->isWatching) {
            throw new \LogicException(
                "You have to start a Component before you can start a slot."
            );
        }
        if ($this->slot) {
            throw new \LogicException(
                "'{$this->name}' has already an active slot: '{$this->slot}'. ".
                "Close it before you can start a new one."
            );
        }
        $this->slot = $name;
        ob_start();
    }

    /**
     * Close the slot section
     *
     * @return void
     */
    public function endSlot()
    {
        if (! $this->slot) {
            throw new \LogicException(
                "In '{$this->name}' you try to close a slot before you opened one."
            );
        }
        $this->slotData[$this->slot] = ob_get_clean();
        $this->slot = null;
    }

    /**
     * Make a new Template
     *
     * @return \League\Plates\Template\Template
     */
    protected function makeTemplate()
    {
        return new Template($this->engine, $this->name);
    }
}
