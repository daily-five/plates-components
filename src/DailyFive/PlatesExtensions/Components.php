<?php

namespace DailyFive\PlatesExtensions;

use DailyFive\Template\Component;
use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

/**
 * Class Components
 * @package App\PlatesExtensions
 */
class Components implements ExtensionInterface
{
    /**
     * @var \League\Plates\Engine
     */
    protected $engine;

    /**
     * List of active Components
     *
     * @var \DailyFive\Template\Component[]
     */
    protected $components = array();

    /**
     * Max nesting level. Default is 0.
     * 0 stands for infinite nesting.
     *
     * @var int
     */
    protected $maxNestingLvl;

    /**
     * Components constructor.
     *
     * @param int $maxNestingLvl
     */
    public function __construct($maxNestingLvl = 0)
    {
        if (! is_int($maxNestingLvl)) {
            throw new \LogicException(
                "Components nesting level must be an integer or null"
            );
        } elseif (! ($maxNestingLvl >= 0)) {
            throw new \LogicException(
                "Components nesting level must be greater or equal than 0."
            );
        }

        $this->maxNestingLvl = $maxNestingLvl;
    }

    /**
     * Register extension function.
     *
     * @param \League\Plates\Engine $engine
     *
     * @return void
     */
    public function register(Engine $engine)
    {
        $engine->registerFunction('component', array($this, 'startComponent'));
        $engine->registerFunction('startComponent', array($this, 'startComponent'));
        $engine->registerFunction('endComponent', array($this, 'endComponent'));

        $engine->registerFunction('slot', array($this, 'startSlot'));
        $engine->registerFunction('startSlot', array($this, 'startSlot'));
        $engine->registerFunction('endSlot', array($this, 'endSlot'));

        $this->engine = $engine;
    }

    /**
     * Start a new Component section
     *
     * @param string $name
     * @param array  $data
     *
     * @return void
     * @throws \LogicException
     */
    public function startComponent($name, array $data = array())
    {
        $this->checkNestingLvl();
        $this->components[] = $this->make($name);
        $this->current()->data($data);
        $this->current()->start();
    }

    /**
     * Close the current Component section
     *
     * @return string
     * @throws \Exception
     * @throws \Throwable
     */
    public function endComponent()
    {
        $this->current()->end();
        $rendered = $this->current()->render();
        $this->removeCurrent();
        return $rendered;
    }

    /**
     * Start a new slot section
     *
     * @param string $name
     *
     * @return void
     */
    public function startSlot($name)
    {
        $this->current()->startSlot($name);
    }

    /**
     * Close the current slot section
     *
     * @return void
     */
    public function endSlot()
    {
        $this->current()->endSlot();
    }

    /**
     * Make a new Component
     *
     * @param string $name
     *
     * @return \DailyFive\Template\Component
     */
    protected function make($name)
    {
        return new Component($this->engine, $name);
    }

    /**
     * Return the current Component
     *
     * @return \DailyFive\Template\Component|null
     */
    public function current()
    {
        return ($count = count($this->components))
            ? $this->components[$count - 1]
            : null;
    }

    /**
     * Remove the current Component
     *
     * @return void
     */
    protected function removeCurrent()
    {
        array_pop($this->components);
    }

    /**
     * Checks the nesting level
     *
     * @return bool
     * @throws \LogicException
     */
    protected function checkNestingLvl()
    {
        if ($this->maxNestingLvl === 0) {
            return true;
        }
        if (count($this->components) >= $this->maxNestingLvl) {
            throw new \LogicException(
                "The max nesting level of {$this->maxNestingLvl} for template Components is reached."
            );
        }
    }
}
