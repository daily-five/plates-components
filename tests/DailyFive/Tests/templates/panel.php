<div>
    <div><?= isset($title) ? $title : '' ?></div>
    <div><?= isset($body) ? $body : '' ?></div>
    <?= isset($slot) ? $slot : '' ?>
</div>