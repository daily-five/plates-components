<?php

namespace Tests\Template;

use DailyFive\Template\Component;
use League\Plates\Engine;
use PHPUnit\Framework\TestCase;

/**
 * Class ComponentTest
 * @package Tests\Template
 */
class ComponentTest extends TestCase
{
    /**
     * @var \League\Plates\Engine
     */
    protected $templateEngine;

    public function setUp()
    {
        $this->templateEngine = new Engine(__DIR__.'/../templates');
    }

    /**
     * @expectedException \LogicException
     */
    public function testUnknownComponentTemplate()
    {
        $component = new Component($this->templateEngine, 'is-not-available');
        $component->render();
    }

    public function testRenderNoContent()
    {
        $c = new Component($this->templateEngine, 'container');
        $this->assertEquals('<div></div>', $c->render());
    }

    public function testRenderDefaultSlot()
    {
        $c = new Component($this->templateEngine, 'container');

        $html = '<p class="h1">Default Slot</p>';

        $c->start();
            echo $html;
        $c->end();

        $this->assertEquals('<div>'.$html.'</div>', $c->render());
    }

    public function testRenderSlots()
    {
        $c = new Component($this->templateEngine, 'panel');

        $title = 'Title';
        $body = '<p>Panel Body</p>';

        $c->start();
            $c->startSlot('title');
                echo $title;
            $c->endSlot();

            $c->startSlot('body');
                echo $body;
            $c->endSlot();
        $c->end();

        $rendered = "<div>\n    <div>{$title}</div>\n    <div>{$body}</div>\n    </div>";

        $this->assertEquals($rendered, $c->render());
    }

    public function testRenderSlotsAndDefaultSlot()
    {
        $c = new Component($this->templateEngine, 'panel');

        $title = 'Title';
        $body = '<p>Panel Body</p>';

        $c->start();
            echo "Hello ";

            $c->startSlot('title');
                echo $title;
            $c->endSlot();

            echo "World";

            $c->startSlot('body');
                echo $body;
            $c->endSlot();

            echo "!";
        $c->end();

        $rendered = "<div>\n    <div>{$title}</div>\n    <div>{$body}</div>\n    Hello World!</div>";

        $this->assertEquals($rendered, $c->render());
    }

    public function testNestedComponents()
    {
        $c = new Component($this->templateEngine, 'container');
        $cn = new Component($this->templateEngine, 'panel');

        $title = 'Title';
        $body = '<p>Panel Body</p>';

        $c->start();
            $cn->start();
                echo "Hello ";

                $cn->startSlot('title');
                    echo $title;
                $cn->endSlot();

                echo "World";

                $cn->startSlot('body');
                    echo $body;
                $cn->endSlot();

                echo "!";
            $cn->end();
            echo $cn->render();
        $c->end();


        $renderedNested = "<div>\n    <div>{$title}</div>\n    <div>{$body}</div>\n    Hello World!</div>";

        $this->assertEquals("<div>{$renderedNested}</div>", $c->render());
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage The Component 'container' has to be started before it ends.
     */
    public function testMissingComponentStart()
    {
        $c = new Component($this->templateEngine, 'container');

        $c->end();
        $c->render();
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage The Component 'container' has to be closed before it can be rendered.
     */
    public function testMissingComponentEnd()
    {
        $c = new Component($this->templateEngine, 'container');

        $c->start();
        ob_end_clean(); // Only necessary for Testing
        $c->render();
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage In 'container' you try to close a slot before you opened one.
     */
    public function testMissingSlotStart()
    {
        $c = new Component($this->templateEngine, 'container');

        $c->start();
        ob_end_clean(); // Only necessary for Testing
            $c->endSlot();
        $c->end();
        $c->render();
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage Slot 'test' has to be closed before you can close the Component 'container'.
     */
    public function testMissingSlotEnd()
    {
        $c = new Component($this->templateEngine, 'container');

        $c->start();
            $c->startSlot('test');
            ob_end_clean(); // Only necessary for Testing
            ob_end_clean(); // Only necessary for Testing
        $c->end();
        $c->render();
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage You have to start a Component before you can start a slot.
     */
    public function testStartASlotBeforeComponent()
    {
        $c = new Component($this->templateEngine, 'container');

        $c->startSlot('test');
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage You try to start 'container' until it's already started.
     */
    public function testStartAComponentStillItIsStarted()
    {
        $c = new Component($this->templateEngine, 'container');

        $c->start();
        ob_end_clean(); // Only necessary for Testing
        $c->start();
    }
}
