<?php

namespace Tests\PlatesExtension;

use DailyFive\PlatesExtensions\Components;
use League\Plates\Engine;
use League\Plates\Template\Template;
use PHPUnit\Framework\TestCase;

/**
 * Class ComponentsTest
 * @package Tests\PlatesExtension
 */
class ComponentsTest extends TestCase
{
    /**
     * @var \League\Plates\Engine
     */
    protected $templateEngine;

    public function setUp()
    {
        $this->templateEngine = new Engine(__DIR__.'/../templates');
    }

    /**
     * @expectedException \LogicException
     */
    public function testNestingLvlNull()
    {
        new Components(null);
    }

    /**
     * @expectedException \LogicException
     */
    public function testNestingLvlIsString()
    {
        new Components('foo');
    }

    /**
     * @expectedException \LogicException
     */
    public function testNestingLvlIsArray()
    {
        new Components(array());
    }

    /**
     * @expectedException \LogicException
     */
    public function testNestingLvlIsBool()
    {
        new Components(false);
    }

    /**
     * @expectedException \LogicException
     */
    public function testNestingLvlIsObject()
    {
        new Components(new Components());
    }

    /**
     * @expectedException \LogicException
     */
    public function testNestingLvlIsNotGreaterOrEqualThenZero()
    {
        new Components(-1);
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage The max nesting level of 1 for template Components is reached.
     */
    public function testMaxNestingLevel()
    {
        $this->templateEngine->loadExtension(new Components(1));

        $t = new Template($this->templateEngine, 'nesting');

        $t->render();
    }

    public function testComponentsNesting()
    {
        $this->templateEngine->loadExtension(new Components());

        $rendered = "<div><div><div></div></div><div></div></div>";

        $this->assertEquals($rendered, $this->templateEngine->render('nesting'));
    }
}
